using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Controller;
using Climbing;

public class PlayerStamina : MonoBehaviour
{
    public float maxStamina = 100f;
    public float moveCost = 10f;
    public float staminaRegen = 20f;
    public float regenDelay = 1f;
    //public Image staminaBar;
    private bool isClimbing = false;
    private StateManager cb;
    private ClimbingBehavior escal;

    [SerializeField] GameObject mainSlider;
    private float currentStamina;
    private float regenTimer;
    private bool canMove = true;

    private void Start()
    {
        currentStamina = maxStamina;
        cb = GetComponent<StateManager>();


        escal = GetComponent<ClimbingBehavior>();


        UpdateStaminaBar();
    }

    private void Update(){
        UpdateStamina() ;       
    }

    public void SpendStamina(float stamina)
    {
        currentStamina -= stamina;
    }
    

    private void UpdateStamina()
    {
        if ((canMove && cb.dummy) || (canMove && cb.sprint || canMove && !cb.onGround))
        {
            if (currentStamina > 0)
            {
                currentStamina -= moveCost * Time.deltaTime;
            }
            else
            {
                Debug.Log("chama");
                canMove = false;
                escal.FallFromClimbing();
                cb.isTired = true;
            }
        }
        else
        {
            if (currentStamina < maxStamina && regenTimer >= regenDelay)
            {
                currentStamina += staminaRegen * Time.deltaTime;

                if (maxStamina < currentStamina)
                    currentStamina = maxStamina;


                if (!canMove)
                {
                    canMove = true;
                    cb.isTired = false;
                }
            }
            else
            {
                regenTimer += Time.deltaTime;
            }
        }
        UpdateStaminaBar();
        
    }

    public void UpdateStaminaBar()
    {
        if(maxStamina != currentStamina)
        {
            if (!mainSlider.activeInHierarchy)
            {
                mainSlider.SetActive(true);
            }
            mainSlider.GetComponent<Slider>().value = currentStamina;
        }
        else
        {
            mainSlider.SetActive(false);
        }

    }
}
