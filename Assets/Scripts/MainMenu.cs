using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private void Start()
    {
        if(!AudioManager.instance.IsPlaying("MainTheme"))
            AudioManager.instance.PlaySound("MainTheme");

        Cursor.visible = true;

    }
    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        //AudioManager.instance.StopSound("MainTheme");
        Debug.Log("Start Playing");
    }

    public void Credit()
    {
        SceneManager.LoadScene("Final");
        //AudioManager.instance.StopSound("MainTheme");
        Debug.Log("Creditos Playing");
    }

    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
