using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

namespace Climbing
{
    public class ClimbingBehavior : MonoBehaviour
    {
        #region Variaveis de controle do comportamento
        // 
        [SerializeField] private bool _isClimbing;
        public bool IsClimbing => _isClimbing;
        private bool _initClimbing;
        private bool _waitToStartClimbing;

        #endregion

        #region Variaveis de Componentes
        private Animator _animator;
        private ClimbIK _climbIk;
        #endregion

        #region Variaveis do ponto
        private Manager _currentManager;
        private Point _targetPoint;
        private Point _currentPoint;
        private Point _previousPoint;
        private Neighbour _neighbour;
        private EnumConnectionType _currentConnectionType;
        #endregion

        #region Variaveis estado atual e almejado
        private EnumClimbState _climbState;
        private EnumClimbState _targetState;
        #endregion

        #region variaveis para movimentos de curva
        private CurvesHolder _curvesHolder;
        private BezierCurve _directCurveHorizontal;
        private BezierCurve _directCurveVertical;   
        private BezierCurve _dismountCurve;
        private BezierCurve _mountCurve;
        private BezierCurve _currentCurve;
        #endregion

        #region variaveis de interpola��o
        private Vector3 _startPosition;
        private Vector3 _targetPosition;
        private float _distance;
        private float _transitionIndex;
        private bool _initTransit;
        private bool _rootReached;
        private bool _ikLandSideReached = false;
        private bool _ikFollowSideReached = false;
        private bool waitForWrapUp;
        #endregion

        #region Variaveis de input
        private bool _lockInput;
        private Vector3 _inputDirection;
        private Vector3 _targetDirection;
        #endregion

        #region variaveis de ajuste
        [Header("variaveis de ajuste")]
        [SerializeField] private float _maxDistanceToPoint = 5f;
        // Y diz respeito a quanto o quadril do personagem fica acima do ch�o
        [SerializeField] private Vector3 _rootOffset = new Vector3(0, -0.85f, 0);
        public Vector3 RootOffset => _rootOffset;
        [SerializeField] private float _speedLinear = 1.3f;
        public float SpeedLinear => _speedLinear;
        [SerializeField] private float _speedDirect = 2f;
        public float SpeedDirect => _speedDirect;

        [SerializeField] private AnimationCurve _animJumpCurve;
        public AnimationCurve AnimJumpCurve => _animJumpCurve;
        [SerializeField] private AnimationCurve _animMountCurve;
        public AnimationCurve AnimateMountCurve => _animMountCurve;
        [SerializeField] public bool _enableRootMovement;


        private float _rootMovementMAx = 0.25f;
        private float _rootMovementT;

        #endregion

        #region vari�veis IK

        private AvatarIKGoal _iklanding;
        private AvatarIKGoal _ikFollowing;
        float _ikT;
        float _followingikT;
        Vector3[] _ikStartPos = new Vector3[4];
        Vector3[] _ikTargetPos = new Vector3[4];
        #endregion
        public void SetCurveReferences()
        {
            // Cria um GameObject com todas as curvas desejadas e as atribui
            GameObject curveHolderPrefab = Resources.Load("CurvesHolder") as GameObject;
            GameObject curveHolderGameObject = Instantiate(curveHolderPrefab) as GameObject;

            _curvesHolder = curveHolderGameObject.GetComponent<CurvesHolder>();

            _directCurveHorizontal = _curvesHolder.ReturnCurve(CurveType.horizontal);
            _directCurveVertical = _curvesHolder.ReturnCurve(CurveType.vertical);
            _dismountCurve = _curvesHolder.ReturnCurve(CurveType.dismount);
            _mountCurve = _curvesHolder.ReturnCurve(CurveType.mount);
        }


        // Start is called before the first frame update
        void Start()
        {
            _animator = GetComponentInChildren<Animator>();
            _climbIk = GetComponentInChildren<ClimbIK>();
            SetCurveReferences();
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void FixedUpdate()
        {
            if (_isClimbing)
            {
                if (!_waitToStartClimbing)
                {
                    // Caso n�o seja o primeiro frame cuida da escalada
                    HandleClimbing();
                    InitiateFallOff();
                }
                else
                {
                    // Inicia a escalada caso seja o primeiro frame
                    InitClimbing();
                    HandleMount();
                }
            }
            else
            {
                if(_initClimbing)
                {
                    transform.parent = null;
                    _initClimbing = false;
                }

                // Poss�vel alterar depois, talvez
                if (Input.GetKey(KeyCode.Space))
                    LoadForClimSpot();
               
            }
            //print(_climbState);
        }

        #region Antes de escalar

        private void LoadForClimSpot()
        {
            
            // adaptar para o nosso controle
            // Dispara um raycast para encontrar uma parede escal�vel 
            Transform cameraTransform = Camera.main.transform;
            Ray ray = new Ray(cameraTransform.position, cameraTransform.forward);

            RaycastHit hit;
            // Ignora certas layers indesejadas
            LayerMask layerMask = (1 << gameObject.layer) | (1<<3);
            layerMask = ~layerMask;

            float maxDistance = 20;

            if(Physics.Raycast(ray, out hit, maxDistance, layerMask))
            {
                // deve encontrar um objeto que retorne e contenha a classe manager, n�o fica no mesmo objeto que o collider
                if (hit.transform.GetComponentInParent<Manager>())
                {
                    
                    Manager targetManager = hit.transform.GetComponentInParent<Manager>();

                    Point closestPoint = targetManager.ReturnClosestPoint(transform.position);

                    float distanceToPoint = Vector3.Distance(transform.position, closestPoint.transform.parent.position);

                    // Se o ponto encontrado estiver a uma distancia pr�xima estipulada, atribui-se o ponto ao player
                    if (distanceToPoint < _maxDistanceToPoint) {
                        _currentManager = targetManager;
                        _targetPoint = closestPoint;
                        _targetDirection = closestPoint.transform.position;
                        _currentPoint = closestPoint;
                        _isClimbing = true;
                        _lockInput = true;
                        _targetState = EnumClimbState.ONPOINT;

                        _animator.CrossFade("To_Climb", 0.4f);
                        GetComponent<Controller.StateManager>().DisableController();

                        _waitToStartClimbing = true;
                    }

                }
            }
        }

        #endregion

        #region Inicio da escalada

        public void InitClimbing()
        {
            if (!_initClimbing)
            {
                _initClimbing = true;
                // atualiza os IK
                
                if(_climbIk != null)
                {
                    // quando a escalada � inicializada todos os ik s�o atualizados de acordo com o ponto alvo
                    _climbIk.UpdateAllPointsOnOne(_targetPoint);
                    _climbIk.UpdateAllTargetPositions(_targetPoint);
                    // como � um pulo e n�o tem seguimento para outro ik usamos essa fun��o
                    _climbIk.ImmediatePlaceHelpers();
                    
                }
                

                // atualiza o tipo das conex�es e estado da escalada
                _currentConnectionType = EnumConnectionType.DIRECT;
                _targetState = EnumClimbState.ONPOINT;
            }
        }
        public void HandleMount()
        {
            // simula um lerp para realizar a transi��o entre dois pontos
            if(!_initTransit)
            {
                // Inicia as configura��es da transi��o
                _initTransit = true;
                _ikFollowSideReached = false;
                _ikLandSideReached = false;
                _enableRootMovement = true;
                _transitionIndex = 0;
                _startPosition = transform.position;
                // offset � necess�rio pois os quadris do modelo n�o ficam na raiz do objeto do player
                _targetPosition = _targetDirection + _rootOffset; 

                _currentCurve = _mountCurve;
                _currentCurve.transform.rotation = _targetPoint.transform.rotation;
                BezierPoint[] bezierPoints = _currentCurve.GetAnchorPoints();
                bezierPoints[0].transform.position = _startPosition;
                bezierPoints[^1].transform.position = _targetPosition;

            }

            // vai atualizando o index da transi��o na curva
            if (_enableRootMovement)
            {
                _transitionIndex += Time.deltaTime*2;
            }

            if (_transitionIndex >= 0.99f)
            {
                _transitionIndex = 1;
                _waitToStartClimbing = false;
                _lockInput = false;
                _initTransit = false;
                _ikLandSideReached = false;
                _climbState = _targetState;
            }

            Vector3 targetPosition = _currentCurve.GetPointAt(_transitionIndex);
            transform.position = targetPosition;

            HandleWeightAll(_transitionIndex, _animMountCurve);

            HandleRotation();

        }

        public void HandleRotation()
        {
            // roda o personagem para olhar para a dire��o do ponto de destino
            Vector3 targetDirection = _targetPoint.transform.forward;

            if (targetDirection == Vector3.zero)
                targetDirection = transform.forward;

            Quaternion targetRotation = Quaternion.LookRotation(targetDirection);

            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 5);
            
        }

        public void HandleWeightAll(float transitionIndex, AnimationCurve animCurve)
        {
            float inf = animCurve.Evaluate(transitionIndex);
            _climbIk.AddWeightInfluenceAll(1 - inf);
        }

        #endregion

        #region Durante a escalada
        public void InitiateFallOff()
        {
            // Inicia a queda do personagem
            if (_climbState == EnumClimbState.ONPOINT)
            {
                if (Input.GetKeyUp(KeyCode.X))
                {
                    FallFromClimbing();
                }
            }
        }

        public void FallFromClimbing()
        {
                _isClimbing = false;
                _initTransit = false;
                _climbIk.AddWeightInfluenceAll(0);
                GetComponent<Controller.StateManager>().EnableController();
                _animator.Play("onAir");
        }

        public void HandleClimbing()
        {
            if (!_lockInput)
            {
                // cuida dos inputs quando o player j� n�o est� se movendo
                _inputDirection = Vector3.zero;

                float horizontal = Input.GetAxis("Horizontal");
                float vertical = Input.GetAxis("Vertical");

                _inputDirection = ConvertToInputDirection(horizontal, vertical);


                if (_inputDirection != Vector3.zero)
                {
                    switch (_climbState)
                    {
                        case EnumClimbState.ONPOINT:
                            OnPoint(_inputDirection);
                            break;
                        case EnumClimbState.BETWEENPOINTS:
                            BetweenPoints(_inputDirection);
                            break;
                        case EnumClimbState.INTRANSIT:
                            break;
                        default:
                            break;
                    }
                }

                // tratamento de posi��o do player para ONPOINT em caso de movimenta��o do ponto
                transform.parent = _currentPoint.transform.parent;

                if (_climbState == EnumClimbState.ONPOINT)
                {
                    _climbIk.UpdateAllTargetPositions(_currentPoint);
                    _climbIk.ImmediatePlaceHelpers();
                }
            }
            else
            {
                InTransit(_inputDirection);
            }
        }

        #region OnPoint
        public void OnPoint(Vector3 inputDirection)
        {
            // Trata o caso em que o player est� em um ponto e pode ir para todas as conex�es do ponto

            // acha um ponto vizinho, se existir, com a dire��o dada
            _neighbour = null;
            _neighbour = _currentManager.ReturnNeighbour(inputDirection, _currentPoint);

            if (_neighbour != null)
            {
                _targetPoint = _neighbour.Target; // coloca o vizinho como alvo
                _previousPoint = _currentPoint; // coloca o ponto anterior como o atual
                _climbState = EnumClimbState.INTRANSIT; // muda o estado para transito

                // atualiza as variaveis de acordo com a conex�o e dire��o do input
                UpdateConnectionTransitionByType(_neighbour, inputDirection);

                _lockInput = true;
            }
            else
            {
                if (_inputDirection == new Vector3(0,-1,0))
                {
                    FallFromClimbing();
                }
                
            }


        }

        public void UpdateConnectionTransitionByType(Neighbour neighbour, Vector3 inputDirection)
        {
            // Gerencia o comportamento do player de acordo com o tipo de conex�o com vizinho e a dire��o do input 
            Vector3 desiredPosition = Vector3.zero;
            _currentConnectionType = neighbour.ConnectionType;

            Vector3 direction = _targetPoint.transform.position - _currentPoint.transform.position;
            direction.Normalize();

            TransitDir transitDirection;

            switch (neighbour.ConnectionType)
            {
                case EnumConnectionType.INBETWEEN:
                    // Se a conex�o tem 2 passos
                    float distance = Vector3.Distance(_currentPoint.transform.position, _targetPoint.transform.position);
                    desiredPosition = _currentPoint.transform.position + (direction * (distance / 2)); // a posi��o desejada fica no meio dos dois pontos
                    _targetState = EnumClimbState.BETWEENPOINTS;
                    transitDirection = ReturnTransitionDirection(inputDirection, false);
                    PlayAnim(transitDirection);
                    break;

                case EnumConnectionType.DIRECT:
                    // Se a conex�o ocorre em apenas um passo (existe pulo), ent�o a curva realiza grande parte do trabalho
                    desiredPosition = _targetPoint.transform.position;
                    _targetState = EnumClimbState.ONPOINT;
                    transitDirection = ReturnTransitionDirection(inputDirection, true);
                    PlayAnim(transitDirection, true);
                    break;

                case EnumConnectionType.DISMOUNT:
                    desiredPosition = _targetPoint.transform.position;
                    _animator.SetInteger("JumpType", 20);
                    _animator.SetBool("Move", true);
                    break;
            }
            _targetDirection = desiredPosition;
        }

        #endregion

        #region BetweenPoints
        public void BetweenPoints(Vector3 inputDirection)
        {
            // Trata o caso em que s� existem 2 pontos possiveis na transi��o, continuar o movimento e voltar para o ponto inicial
            Neighbour neighbour = _targetPoint.ReturnNeighbour(_previousPoint);

            if (neighbour != null)
            {
                if (inputDirection == neighbour.Direction)
                    _targetPoint = _previousPoint;
            }
            else
            {
                _targetPoint = _currentPoint;
            }

            _targetDirection = _targetPoint.transform.position;
            _climbState = EnumClimbState.INTRANSIT;
            _targetState = EnumClimbState.ONPOINT;
            _previousPoint = _currentPoint;
            _lockInput = true;
            _animator.SetBool("Move", false);
        }
        #endregion

        #region inTransit
        public void InTransit(Vector3 inputDirection)
        {
            switch (_currentConnectionType)
            {
                case EnumConnectionType.INBETWEEN:
                    if (!AudioManager.instance.IsPlaying("escalada") && !_initTransit)
                        AudioManager.instance.PlaySound("escalada");
                    UpdateLinearVariables();
                    LinearRootMovement();
                    LerpIkLandingSiteLinear();
                    WrapUp();
                    
                    break;
                case EnumConnectionType.DIRECT:
                    if (!AudioManager.instance.IsPlaying("pulo_escalada") && !_initTransit)
                        AudioManager.instance.PlaySound("pulo_escalada");
                    UpdateDirectVariables(inputDirection);
                    DirectRootMovement();
                    DirectHandleIk();
                    WrapUp(true);
                    break;
                case EnumConnectionType.DISMOUNT:
                    UpdateDismountVariables();
                    DismountRootMovement();
                    DismoutHandleIk();
                    DismountWrapUp();
                    break;
            }
              
        }

        



        #region To Dismount
        private void UpdateDismountVariables()
        {
            if (!_initTransit)
            {
                _initTransit = true;
                _enableRootMovement = false;
                _rootReached = false;
                _ikFollowSideReached = false;
                _ikLandSideReached = false;
                _transitionIndex = 0;
                _rootMovementT = 0;
                _startPosition = transform.position;
                _targetPosition = _targetDirection;

                // atualiza os pontos na curva a ser usada
                _currentCurve = _dismountCurve;
                BezierPoint[] points = _currentCurve.GetAnchorPoints();
                _currentCurve.transform.rotation = transform.rotation;
                points[0].transform.position = _startPosition;
                points[points.Length - 1].transform.position = _targetPosition;

                _ikT = 0;
                _ikT = 0;
            }
            
        }

        private void DismountRootMovement()
        {
            if (_enableRootMovement)
                _transitionIndex += Time.deltaTime / 2;

            if (_transitionIndex >= 0.99f)
            {
                _transitionIndex = 1;
                _rootReached = true;
            }

            Vector3 targetPosition = _currentCurve.GetPointAt(_transitionIndex);
            transform.position = targetPosition;
        }

        

        private void DismountWrapUp()
        {
            if (_rootReached)
            {
                _animator.SetBool("Move", false);
                _inputDirection = Vector3.zero;
                _currentManager = null;
                _neighbour = null;
                _isClimbing = false;
                _initTransit = false;
                _climbIk.AddWeightInfluenceAll(0);
                GetComponent<Controller.StateManager>().EnableController();
            }
        }
        #endregion

        #region To InBetween
        
        private void LinearRootMovement()
        {
            // vai fazer um lerp para a a posi��o alvo

            float speed = _speedLinear * Time.deltaTime;
            float lerpSpeed = speed / _distance;
            _transitionIndex += lerpSpeed;

            if (_transitionIndex > 1)
            {
                _transitionIndex = 1;
                _rootReached = true;
            }

            Vector3 currentPosition = Vector3.LerpUnclamped(_startPosition,_targetPosition,_transitionIndex);
            transform.position = currentPosition;

            HandleRotation();
        }

        public void UpdateLinearVariables()
        {
            // Atualiza as vari�veis para a movimenta��o linear do estado In Between
            if (!_initTransit)
            {
                _initTransit = true;
                _enableRootMovement = true;
                _rootReached = false;
                _ikFollowSideReached = false;
                _ikLandSideReached = false;
                _transitionIndex = 0;
                _startPosition = transform.position;
                _targetPosition = _targetDirection + _rootOffset;
                
                Vector3 directionToPoint = (_targetPosition - _startPosition).normalized;

                bool twoStep = (_targetState == EnumClimbState.BETWEENPOINTS);
                Vector3 backPosition = -transform.forward * 0.05f;
                if (twoStep) 
                    _targetPosition += backPosition;

                _distance = Vector3.Distance(_targetPosition, _startPosition);

                InitIk(directionToPoint, !twoStep);

            }
        }



        public void WrapUp(bool direct = false)
        {
            // adiciona delay se necess�rio para o movimento

            // � poss�vel adicionar mais uma condi��o if para quando os Iks tenham terminado, estudar depois
            if(_rootReached)
            {
                if (!_animator.GetBool("Jump"))
                {
                    if (!waitForWrapUp)
                    {
                        StartCoroutine(WrapUpTransition(0.05f));
                        waitForWrapUp = true;
                    }
                }
            }

        }

        IEnumerator WrapUpTransition(float time)
        {
            yield return new WaitForSeconds(time);

            _climbState = _targetState; // ajeitando o estado atual

            if (_climbState == EnumClimbState.ONPOINT)
                _currentPoint = _targetPoint; // S� atualiza quando o jogador chega no ponto alvo

            // reseta as vari�veis
            _initTransit = false;
            _lockInput = false;
            _inputDirection = Vector3.zero;
            waitForWrapUp = false;
            
        }
        #endregion

        #region To Direct


        private void DirectRootMovement()
        {
            // n�o realiza o lerp, deixa o trabalho para a curva, deixa o movimento mais natural
            if (_enableRootMovement)
            {
                _transitionIndex += Time.deltaTime * SpeedDirect;
            }
            else
            {
                if (_rootMovementT < _rootMovementMAx)
                    _rootMovementT += Time.deltaTime;
                else
                    _enableRootMovement = true;
            }

            if (_transitionIndex > 0.99f)
            {
                _transitionIndex = 1;
                _rootReached = true;
            }

            HandleWeightAll(_transitionIndex, _animJumpCurve);

            Vector3 targetPosition = _currentCurve.GetPointAt(_transitionIndex);
            transform.position = targetPosition;

            HandleRotation();


        }

        private void UpdateDirectVariables(Vector3 inputDirection)
        {
            if (!_initTransit)
            {
                _initTransit = true;
                _enableRootMovement = true;
                _rootReached = false;
                _ikFollowSideReached = false;
                _ikLandSideReached = false;
                _transitionIndex = 0;
                _rootMovementT = 0;
                _startPosition = transform.position;
                _targetPosition = _targetDirection + _rootOffset;

                // A curva a ser utilizada depende se o movimento � vertical ou horizontal

                // Verifica a dire��o do movimento
                bool vertical = (Math.Abs(inputDirection.y) > 0.1f);
                _currentCurve = (vertical) ? _directCurveVertical : _directCurveHorizontal;
                _currentCurve.transform.rotation = _currentPoint.transform.rotation;

                if (!vertical)
                {
                    if (!(inputDirection.x > 0) )// Movimenta��o para esquerda
                    {
                        Vector3 eulers = _currentCurve.transform.eulerAngles;
                        eulers.y = -180;
                        _currentCurve.transform.eulerAngles = eulers;
                    }
                }
                else
                {
                    if (!(inputDirection.y > 0)) // movimenta��o para baixo
                    {
                        Vector3 eulers = _currentCurve.transform.eulerAngles;
                        eulers.x = 180;
                        eulers.y = 180;
                        _currentCurve.transform.eulerAngles = eulers;
                    }
                }

                // ajeitar o primeiro ponto da curva na posi��o inicial
                // e o ultimo na posi��o alvo
                BezierPoint[] points = _currentCurve.GetAnchorPoints();
                points[0].transform.position = _startPosition;
                points[points.Length-1].transform.position = _targetPosition;




                InitIk_Direct(inputDirection);

            }
        }

       
        #endregion

        #endregion

        #endregion

        #region Input
        public Vector3 ConvertToInputDirection(float horizontal, float vertical)
        {
            // se horizontal for diferente de 0, recebe -1 ou 1, de acordo com o valor do horizontal
            int h = (horizontal != 0) ? (horizontal < 0) ? -1 : 1 : 0;
            // se vertical for diferente de 0, recebe -1 ou 1, de acordo com o valor do vertical
            int v = (vertical != 0) ? (vertical < 0) ? -1 : 1 : 0;

            int z = v + h;

            // para alguns casos especificos quandose deseja ir para frente
            z = (z != 0) ? (z < 0) ? -1 : 1 : 0;


            return new Vector3(h, v);


        }

        #endregion

        #region Anima��o

        public TransitDir ReturnTransitionDirection(Vector3 inputDirection, bool jump)
        {
            // gerencia o tipo de anima��o que ser� tocado sem ter que se preocupar muito com vari�veis no animator
            TransitDir transitDirection = default;

            float targetAngle = Mathf.Atan2(inputDirection.x, inputDirection.y) * Mathf.Rad2Deg;

            if (!jump)
            {
                if(Mathf.Abs(inputDirection.y) > 0)
                {
                    transitDirection = TransitDir.m_vert;
                }
                else
                {
                    transitDirection= TransitDir.m_hor;
                }
            }
            else
            {
                // Verifica pra qual das 8 possiveis dire��es o input direction aponta
                if (targetAngle < 22.5f && targetAngle > -22.5f)
                {
                    transitDirection = TransitDir.j_up;
                }
                else if(targetAngle < 180 + 22.5f && targetAngle > 180 - 22.5f) 
                {
                    transitDirection = TransitDir.j_down;
                }
                else if (targetAngle < 90 + 22.5f && targetAngle > 90 - 22.5f)
                {
                    transitDirection = TransitDir.j_right;
                }
                else if (targetAngle < -90 + 22.5f && targetAngle > -90 - 22.5f)
                {
                    transitDirection = TransitDir.j_left;
                }

                if (Mathf.Abs(inputDirection.y) > Mathf.Abs(inputDirection.x))
                {
                    if (inputDirection.y < 0)
                    {
                        transitDirection = TransitDir.j_down;
                    }
                    else
                    {
                        transitDirection = TransitDir.j_up;
                    }
                }
            }

            return transitDirection;
        }

        public void PlayAnim(TransitDir transitDirection, bool jump = false)
        {
            // traduz as dire��es da tranis��o para o animator, de acordo com a dire��o e tipo do pulo

            int target = 0;

            switch(transitDirection)
            {
                case TransitDir.j_up:
                    target = 0;
                    break; 
                case TransitDir.j_down:
                    target = 1;
                    break; 
                case TransitDir.j_right:
                    target = 2;
                    break;
                case TransitDir.j_left:
                    target = 3;
                    break;
                case TransitDir.m_hor:
                    target = 5;
                    break;
                case TransitDir.m_vert:
                    target = 6;
                    break;
                default:
                    break;
            }
            _animator.SetInteger("JumpType", target);

            if (!jump)
            {
                _animator.SetBool("Move", true);
            }
            else
            {
                _animator.SetBool("Jump", true);
            }

        }

        #endregion

        #region IKs

        
        
        private void InitIk(Vector3 directionToPoint, bool opposite)
        {
            Vector3 relativeDirection = transform.InverseTransformDirection(directionToPoint);

            if (Mathf.Abs(relativeDirection.y) > 0.5f)
            {
                float targetAnimation;

                if( _targetState == EnumClimbState.ONPOINT)
                {
                    _iklanding = _climbIk.ReturnOppositeIK(_iklanding);
                }
                else
                {
                    if (Mathf.Abs(relativeDirection.x) > 0)
                    {
                        if (relativeDirection.x < 0)
                            _iklanding = AvatarIKGoal.LeftHand;
                        else
                            _iklanding = AvatarIKGoal.RightHand;
                    }

                    targetAnimation = (_iklanding == AvatarIKGoal.RightHand) ? 1 : 0;
                    if (relativeDirection.y < 0)
                    {
                        targetAnimation = (_iklanding == AvatarIKGoal.RightHand) ? 0 : 1;
                    }

                    _animator.SetFloat("Movement", targetAnimation);
                }


            }
            else
            {
                _iklanding = (relativeDirection.x < 0) ? AvatarIKGoal.LeftHand : AvatarIKGoal.RightHand;

                if (opposite)
                {
                    _iklanding = _climbIk.ReturnOppositeIK(_iklanding);
                }
            }

            _ikT = 0;
            UpdateIkTarget(0, _iklanding, _targetPoint);

            _ikFollowing = _climbIk.ReturnOppositeLimb(_iklanding);
            _followingikT = 0;
            UpdateIkTarget(1, _ikFollowing, _targetPoint);

        }

        private void UpdateIkTarget(int posIndex, AvatarIKGoal ikGoal, Point targetPoint)
        {
            // atualiza os iks da posi��o atual para a posi��o alvo
            _ikStartPos[posIndex] = _climbIk.ReturnCurrentPointPosition(ikGoal);
            _ikTargetPos[posIndex] = targetPoint.ReturnIK(ikGoal).Target.transform.position;
            _climbIk.UpdatePoint(ikGoal, targetPoint);
        }

        private void LerpIkLandingSiteLinear()
        {
            float speed = _speedLinear * Time.deltaTime;
            float lerpSpeed = speed / _distance;

            _ikT += lerpSpeed * 2;

            if(_ikT > 1)
            {
                _ikT = 1;
                _ikLandSideReached = true;
            }

            Vector3 ikPosition = Vector3.LerpUnclamped(_ikStartPos[0], _ikTargetPos[0], _ikT);
            _climbIk.UpdateTargetPosition(_iklanding, ikPosition);

            _followingikT += lerpSpeed * 2;

            if (_followingikT > 1)
            {
                _followingikT = 1;
                _ikFollowSideReached = true;
            }

            Vector3 followSide = Vector3.LerpUnclamped(_ikStartPos[1], _ikTargetPos[1], _followingikT);
            _climbIk.UpdateTargetPosition(_ikFollowing, followSide);

        }

        private void InitIk_Direct(Vector3 directionToPoint)
        {
            if(directionToPoint.y != 0)
            {
                _followingikT = 0;
                _ikT = 0;

                UpdateIkTarget(0, AvatarIKGoal.LeftHand, _targetPoint);
                UpdateIkTarget(1, AvatarIKGoal.LeftFoot, _targetPoint);

                UpdateIkTarget(2, AvatarIKGoal.RightHand, _targetPoint);
                UpdateIkTarget(3, AvatarIKGoal.RightFoot, _targetPoint);

            }
            else
            {
                InitIk(directionToPoint, false);
                InitIkOpposite();
            }
        }

        private void InitIkOpposite()
        {
            UpdateIkTarget(2, _climbIk.ReturnOppositeIK(_iklanding), _targetPoint);
            UpdateIkTarget(3, _climbIk.ReturnOppositeIK(_iklanding), _targetPoint);
        }


        private void DirectHandleIk()
        {
            if(_inputDirection.y !=0)
            {
                //LerpIkHands_Direct();
                //LerpIkFeet_Direct();
            }
            else
            {
                //LerpIkLandingSide_Direct();
                //LerpIkFollowSide_Direct();
            }
        }

        private void DismoutHandleIk()
        {
            if(_enableRootMovement)
            {
                _ikT += Time.deltaTime * 3;
            }

            _followingikT += Time.deltaTime * 2;

            HandleIKWeight_Dismount(_ikT, _followingikT, 1, 0);
        }

        private void HandleIKWeight_Dismount(float ht, float ft, int from, int to)
        {
            float t1 = ht * 3;

            if(t1 > 1)
            {
                t1 = 1;
                _ikFollowSideReached = true;
            }

            float handsWeight = Mathf.Lerp(from, to, t1);
            _climbIk.InfluenceWeight(AvatarIKGoal.LeftHand, handsWeight);
            _climbIk.InfluenceWeight(AvatarIKGoal.RightHand, handsWeight);

            float t2 = ft * 1;

            if (t2 > 1)
            {
                t2 = 1;
                _ikFollowSideReached = true;
            }

            float feetWeight = Mathf.Lerp(from, to, t2);
            _climbIk.InfluenceWeight(AvatarIKGoal.LeftFoot, feetWeight);
            _climbIk.InfluenceWeight(AvatarIKGoal.RightFoot, feetWeight);

            

        }
        public void EnableRootMovement()
        {
            _enableRootMovement = true;
        }


        #endregion
    }


    public enum EnumClimbState
    {
        ONPOINT, // pode se mover para outros pontos
        BETWEENPOINTS, // esta no meio da transi��o de dois pontos
        INTRANSIT // entre os dois estados
    }

    public enum TransitDir
    {
        m_hor,
        m_vert,
        j_up,
        j_down,
        j_left,
        j_right,
    }
}