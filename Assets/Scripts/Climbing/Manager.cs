using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace Climbing
{
    public class Manager : MonoBehaviour
    {
        [SerializeField] private List<Point> _allPoints = new List<Point>();
        public List<Point> AllPoints => _allPoints;

        // Start is called before the first frame update
        void Start()
        {
            PopulateAllPoints();
        }

        public void Init(){
            PopulateAllPoints();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void PopulateAllPoints(){
            Point[] allPoints = GetComponentsInChildren<Point>();

            foreach (Point point in allPoints)
            {
                if (!_allPoints.Contains(point))
                {
                    _allPoints.Add(point);
                }
            }
        }

        public Point ReturnNeighbourPointFromDirection(Vector3 inputDirection, Point currentPoint)
        {
            Point neighbourPoint = null;

            foreach (Neighbour neighbour in currentPoint.Neighbours)
            {
                if (neighbour.Direction == inputDirection)
                {
                    neighbourPoint = neighbour.Target;
                }
            }

            return neighbourPoint;
        }

        // Retorna o vizinho de acordo com a dire��o do input
        public Neighbour ReturnNeighbour(Vector3 inputDirection, Point currentPoint)
        {
            Neighbour returnNeighbour = null;

            foreach (Neighbour neighbour in currentPoint.Neighbours)
            {
                if (neighbour.Direction == inputDirection)
                {
                    returnNeighbour = neighbour;
                }
            }

            return returnNeighbour;
        }

        public Point ReturnClosestPoint(Vector3 from) {
            Point closestPoint = null;

            float minDistance = Mathf.Infinity;

            foreach (Point point in _allPoints)
            {
                float distance = Vector3.Distance(point.transform.position, from);
                if (distance < minDistance)
                {
                    closestPoint = point;
                    minDistance = distance;
                }
            }

            return closestPoint;
        }


    }
}

