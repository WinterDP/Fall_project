#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Climbing
{
    [ExecuteInEditMode]
    public class DrawLineIndividual : MonoBehaviour
    {
        [SerializeField] private List<Neighbour> _connectedPoints = new List<Neighbour>();
        public List<Neighbour> ConnectedPoints => _connectedPoints;

        [SerializeField] private bool _refresh = false;
        public bool Refresh{
            get => _refresh;
            set => _refresh = value;
        }

        // Update is called once per frame
        void Update()
        {
            if(_refresh){
                _connectedPoints.Clear();
                _refresh = false;
            }
        }
    }

}

#endif
