using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Climbing
{
    [ExecuteInEditMode]    
    public class HandlePoints : MonoBehaviour
    {
        [Header("Helper Properties")]
        [SerializeField] private float _pointDistance = 0.5f;
        [SerializeField] private bool _dismountPoint;
        public bool DismountPoint => _dismountPoint;
        [SerializeField] private bool _fallPoint;
        [SerializeField] private bool _hangingPoint;
        [SerializeField] private bool _singlePoint;


        [Header("clicar após cada mudança")]
        [SerializeField] private bool _updatePoints;

        [Header("Helper utilities")]
        [SerializeField] private bool _deleteAll;
        [SerializeField] private bool _crateIndicators;

        [SerializeField] private GameObject _pointPrefab;

        [SerializeField] private Point _furtherLeft;
        [SerializeField] private Point _furtherRight;

        private List<Point> _pointsInOrder = new List<Point>();
        public List<Point> PointsInOrder => _pointsInOrder;

        public void HandlePrefab(){
            _pointPrefab = Resources.Load("Point") as GameObject;
            if (_pointPrefab == null)
            {
                Debug.Log(" prafab do ponto não encontrado");
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (_updatePoints)
            {
                HandlePrefab();
                UpdatePoints();
                _updatePoints = false;
            }

            if(_crateIndicators){
                HandlePrefab();

                if (!_singlePoint)
                {
                    CreateIndicators();
                }
                else
                {
                    CreateSingleIndicator();
                }

                _crateIndicators = false;
            }

            if(_deleteAll){
                DeleteAll();
                _deleteAll = false;
            }
            
        }

        public void UpdatePoints(){
            Point[] points = GetComponentsInChildren<Point>();

            if (_singlePoint)
            {
                _pointsInOrder = new List<Point>();

                foreach (Point point in points)
                {
                    _pointsInOrder.Add(point);
                }
                return;
            }

            if (points.Length < 1)
            {
                Debug.Log("Pontos de limite não encontrados");
                return;
            }

            DeletePrevious(points, _furtherLeft, _furtherRight);

            points = null;
            points = GetComponentsInChildren<Point>();

            CreatePoints(_furtherLeft, _furtherRight);
        }

        public void DeletePrevious(Point[] points, Point furtherLeft, Point furtherRight){

            // destroi todos os pontos entre os limites a direita e a esquerda
            foreach (Point point in points)
            {
                if (point != furtherLeft && point != furtherRight)
                {
                    DestroyImmediate(point.gameObject.transform.parent.gameObject);
                }
            }
        }

        public void CreatePoints(Point furtherLeft, Point furtherRight){
            // Calcula a distancia do ponto limite da direita e da esquerda
            float distLeftToright = Vector3.Distance(GetPosition(furtherLeft), GetPosition(furtherRight));
            // Calcula o numero de pontos que cabem no trecho
            int pointCount = Mathf.FloorToInt(distLeftToright / _pointDistance);
            Vector3 pointDirection = GetPosition(furtherRight) - GetPosition(furtherLeft);
            pointDirection.Normalize();
            // Cria um vetor com o tamanho de pontos que cabem no trecho
            Vector3[] positions = new Vector3[pointCount];

            float interval = 0;
            _pointsInOrder = new List<Point>();
            // inicia a criação dos pontos na esquerda e vai até o ponto mais a direita
            _pointsInOrder.Add(furtherLeft);

            for (int i = 0; i < pointCount; i++)
            {
                interval += _pointDistance;
                positions[i] = GetPosition(furtherLeft) + (pointDirection * interval);

                if (Vector3.Distance(positions[i], GetPosition(furtherRight)) > _pointDistance)
                {
                    GameObject point = Instantiate(_pointPrefab, positions[i],_furtherLeft.transform.rotation) as GameObject;
                    point.transform.parent = transform;
                    _pointsInOrder.Add(point.GetComponentInChildren<Point>());
                }
                else
                {
                    furtherRight.transform.parent.transform.localPosition = transform.InverseTransformPoint(positions[i]);

                    break;
                }
            }

            // Adiciona o ultimo ponto a direita
            _pointsInOrder.Add(furtherRight);
        }

        public Vector3 GetPosition(Point point){
            return point.transform.parent.position;
        }

        public void CreateIndicators(){
            // cria dois indicadores

            GameObject leftPoint = Instantiate(_pointPrefab) as GameObject;
            GameObject rightPoint = Instantiate(_pointPrefab) as GameObject;

            // left point
            leftPoint.transform.parent = transform;
            //move um pouco a esquerda
            leftPoint.transform.localPosition = -(Vector3.right / 2);
            leftPoint.transform.localEulerAngles = Vector3.zero;
            _furtherLeft = leftPoint.GetComponentInChildren<Point>();

            // right point
            rightPoint.transform.parent = transform;
            //move um pouco a direita
            rightPoint.transform.localPosition = (Vector3.right / 2);
            rightPoint.transform.localEulerAngles = Vector3.zero;
            _furtherRight = rightPoint.GetComponentInChildren<Point>();
        }

        public void CreateSingleIndicator(){
            // cria apenas um indicador

            GameObject leftPoint = Instantiate(_pointPrefab) as GameObject;
            leftPoint.transform.parent = transform;
            leftPoint.transform.localPosition = Vector3.zero;
            leftPoint.transform.localEulerAngles = Vector3.zero;
        }

        public void DeleteAll(){
            // pega os pontos e destroi todos
            Point[] points = GetComponentsInChildren<Point>();

            foreach (Point point in points)
            {
                DestroyImmediate(point.transform.parent.gameObject);
            }
        }
    }
    
}
