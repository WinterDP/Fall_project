#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Climbing
{
    [ExecuteInEditMode]
    public class HandlePointConnections : MonoBehaviour
    {
        [Header("configuração das conexões")]
        // máxima distancia entre dois pontos para eles serem conectados
        [SerializeField] private float _maxDistance = 2.5f;
        // determina se a transição será de pulo ou normal para o novo ponto
        [SerializeField] private float _directThreshold = 1f;
        [Header("configurar conexões")]
        [SerializeField] private bool updateConnections = false;
        // ativa a função que limpa todos os pontos na hierarquia
        [SerializeField] private bool resetConnections = false;
        
        
        private List<Point> _allPoints = new List<Point>();
        Vector3[] _availableDirections = new Vector3[8];


        [SerializeField] private List<GameObject> _dismountPoints = new List<GameObject>();
        public void CreateDirection(){
            _availableDirections[0] = new Vector3(1,0,0); //right
            _availableDirections[1] = new Vector3(-1,0,0); //left
            _availableDirections[2] = new Vector3(0,1,0); // up
            _availableDirections[3] = new Vector3(0,-1,0); // down
            _availableDirections[4] = new Vector3(-1,-1,0); // left-down
            _availableDirections[5] = new Vector3(1,1,0); // up-right
            _availableDirections[6] = new Vector3(1,-1,0); // down-right
            _availableDirections[7] = new Vector3(-1,1,0); // up-left
            
        }

        // Update is called once per frame
        void Update()
        {
            if (EditorApplication.isPlaying && Application.isEditor) 
                return;

            if (updateConnections)
            {
                Debug.Log("check");
                ClearGarbage();
                GetPoints();
                CreateDirection();
                CreateConnections();
                FindDismountCandidates();
                //FindFallCandidates();
                RefreshAll();
                updateConnections = false;
            }

            if (resetConnections)
            {
                ClearGarbage();
                GetPoints();
                foreach (Point point in _allPoints)
                {
                    List<Neighbour> customConnections = new List<Neighbour>();

                    foreach (Neighbour neighbour in point.Neighbours)
                    {
                        if (neighbour.CustomConnection)
                        {
                            customConnections.Add(neighbour);
                        }
                    }

                    point.Neighbours.Clear();

                    point.Neighbours.AddRange(customConnections);
                }
                RefreshAll();
                resetConnections = false;
            }
            updateConnections = false;
            resetConnections = false;
        }

        
        private void ClearGarbage()
        {
            foreach (GameObject go in _dismountPoints)
            {
                DestroyImmediate(go);
            }
            _dismountPoints.Clear();
        }

        // Pega os pontos do quadril de todos os objetos filhos
        public void GetPoints(){
            _allPoints.Clear();
            Point[] hipPoints = GetComponentsInChildren<Point>();
            _allPoints.AddRange(hipPoints);
        }

        public void CreateConnections(){
            foreach (Point point in _allPoints)
            {
                Point currentPoint = point;

                foreach (Vector3 direction in _availableDirections)
                {
                    // procura todos os pontos possíveis em uma dada direção
                    List<Point> candidatePoints = CandidatePointsOnDirection(direction, currentPoint);

                    Point closest = ReturnClosestPoint(candidatePoints, currentPoint);

                    if (closest != null)
                    {
                        if(Vector3.Distance(currentPoint.transform.position, closest.transform.position) < _maxDistance){
                            /*
                                Pulos na diagonal podem causar alguns problemas, caso seja o caso pode-se se remover os pulos com o trecho abaixo

                            if (Mathf.Abs(direction.x) > 0 && Mathf.Abs(direction.y) > 0)
                            {
                                if (Vector3.Distance(currentPoint.transform.position, closest.transform.position) > _directThreshold)
                                {
                                    continue;
                                }
                            }
                            */
                            

                            AddNeighbour(currentPoint, closest, direction);

                        }
                    }


                }
            }
        }

        public List<Point> CandidatePointsOnDirection(Vector3 targetDirection, Point from ){
            List<Point> returnPointList = new List<Point>();

            // Verifica da lista de pontos, quais pontos são possíveis de ser alcançados em determinada região. Projeta-se uma área na direção dada e retorna os angulos válidos
            foreach (Point point in _allPoints)
            {
               Point targetPoint = point;

               Vector3 direction = targetPoint.transform.position - from.transform.position;
               Vector3 relativeDirection = from.transform.InverseTransformDirection(direction);

               if (IsDirectuinValid(targetDirection, relativeDirection))
               {
                    returnPointList.Add(targetPoint);
               }
            }

            return returnPointList;
        }

        public bool IsDirectuinValid(Vector3 targetDirection, Vector3 candidate){
            // Garante que a direção não englobe pontos fora do offset. Divide 360 graus entre as 8 direções possíveis (360/8 = 45 então o offset é de 22.5)
            float offset = 22.5f;

            float targetAngle = Mathf.Atan2(targetDirection.x, targetDirection.y) * Mathf.Rad2Deg;
            float angle = Mathf.Atan2(candidate.x,candidate.y) * Mathf.Rad2Deg;

            if(angle < targetAngle + offset && angle > targetAngle - offset){
                return true;
            }

            return false;
        }

        public Point ReturnClosestPoint(List<Point> candidatePoints, Point from){
            // retorna o ponto mais perto de uma referencia da lista de candidatos 
            Point closestPoint = null;
            float minDist = Mathf.Infinity;

            foreach (Point point in candidatePoints)
            {
                float pointDistance = Vector3.Distance(point.transform.position, from.transform.position);

                if (pointDistance < minDist && point != from)
                {
                    minDist = pointDistance;
                    closestPoint = point;
                }
            }

            return closestPoint;
        }

        public void AddNeighbour(Point from, Point target, Vector3 targetDirection){
            Neighbour neighbour = new Neighbour();
            neighbour.Direction = targetDirection;
            neighbour.Target = target;
            // determina o tipo de conexão de 2 pontos  de acordo com a distância
            neighbour.ConnectionType = (Vector3.Distance(from.transform.position, target.transform.position) < _directThreshold ? EnumConnectionType.INBETWEEN : EnumConnectionType.DIRECT);

            from.Neighbours.Add(neighbour);

            // Mantem os dados mesmo fora do editor
            UnityEditor.EditorUtility.SetDirty(from);
        }

        public void FindDismountCandidates(){
            GameObject dismountPrefab = Resources.Load("Dismount") as GameObject;
            if (dismountPrefab == null)
            {
                Debug.Log(" prafab de desmontagem não encontrado");
            }

            HandlePoints[] handlePoints = GetComponentsInChildren<HandlePoints>();

            List<Point> candidates = new List<Point>();

            // Pega todos os pontos que são marcados como pontos de saida para plataforma e adiciona na lista de candidatos
            foreach (HandlePoints handlePoint in handlePoints)
            {
                if (handlePoint.DismountPoint)
                {
                    candidates.AddRange(handlePoint.PointsInOrder);
                }
            }

            if (candidates.Count > 0)
            {
                // cria um objeto para cuidar dos pontos de saida para plataforma e ajeita sua posição
                GameObject parentObj = new GameObject();
                parentObj.name = "Dismount points";
                parentObj.transform.parent = transform;
                parentObj.transform.localPosition = Vector3.zero;
                parentObj.transform.position = candidates[0].transform.position;

                // Para cada ponto adiciona-se duas conexões, uma para o ponto e outra para o ponto de descida 
                foreach (Point point in candidates)
                {
                    Transform worldPosition = point.transform.parent;
                    GameObject dismountObject = Instantiate(dismountPrefab, worldPosition.position, worldPosition.rotation) as GameObject;
                    
                    // Ajeita a posição do objeto para descida do boneco, creio que pode dar problema dps devido aos números mágicos
                    Vector3 targetPosition = worldPosition.position + ((worldPosition.forward/1.6f) + Vector3.up * 1.2f);
                    dismountObject.transform.position = targetPosition;

                    Point dismountPoint = dismountObject.GetComponentInChildren<Point>();

                    Neighbour neighbour = new Neighbour();
                    neighbour.Direction = Vector3.up;
                    neighbour.Target = dismountPoint;
                    neighbour.ConnectionType = EnumConnectionType.DISMOUNT;
                    point.Neighbours.Add(neighbour);

                    Neighbour neighbour2 = new Neighbour();
                    neighbour2.Direction = - Vector3.up;
                    neighbour2.Target = point;
                    neighbour2.ConnectionType = EnumConnectionType.DISMOUNT;
                    dismountPoint.Neighbours.Add(neighbour2);
                    
                    dismountObject.transform.parent = parentObj.transform;

                    RaycastHit hit;
                    if (Physics.Raycast(dismountObject.transform.position, -Vector3.up,out hit, 2))
                    {
                        Vector3 gp = hit.point;
                        gp.y += 0.4f + Mathf.Abs(dismountPoint.transform.localPosition.y);
                        dismountObject.transform.position = gp;
                    }

                    _dismountPoints.Add(dismountObject);
                }
            }
        }

        private void FindFallCandidates()
        {
            //throw new NotImplementedException();
        }


        public void RefreshAll(){
            DrawLine drawLine = transform.GetComponent<DrawLine>();

            if (drawLine != null)
            {
                drawLine.Refresh = true;
            }

            foreach (Point point in _allPoints)
            {
                DrawLineIndividual drawLineIndividual = point.transform.GetComponent<DrawLineIndividual>();
                if (drawLineIndividual != null)
                {
                    drawLineIndividual.Refresh = true;
                }
            }
        }

        public List<Connection> GetAllConnections(){
            List<Connection> returnValue = new List<Connection>();

            foreach (Point point in _allPoints)
            {
                foreach (Neighbour neighbour in point.Neighbours)
                {
                    Connection connection = new Connection();
                    connection.Target1 = point;
                    connection.Target2 = neighbour.Target;
                    connection.ConnectionType = neighbour.ConnectionType;

                    if (!ContainsConnection(returnValue,connection))
                    {
                        returnValue.Add(connection);
                    }
                }
            }
            return returnValue;
        }

        public bool ContainsConnection(List<Connection> connections, Connection connection){
            foreach (Connection conectionInList in connections)
            {
                if(conectionInList.Target1 == connection.Target1 && conectionInList.Target2 == connection.Target2 || conectionInList.Target2 == connection.Target1 && conectionInList.Target1 == connection.Target2){
                    return true;
                }
            }

            return false;
        }


    }

    public class Connection {
        private Point _target1;
        public Point Target1 {
            get => _target1;
            set => _target1 = value;
        }
        private Point _target2;
        public Point Target2 {
            get => _target2;
            set => _target2 = value;
        }
        private EnumConnectionType _connectionType;
        public EnumConnectionType ConnectionType{
            get => _connectionType;
            set => _connectionType = value;
        }
    }

}   
#endif
