using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Climbing
{
    [System.Serializable]
    public class Point : MonoBehaviour
    {
        // Lista de pontos vizinhos
        [SerializeField] private List<Neighbour> _neighbours = new List<Neighbour>();
        public List<Neighbour> Neighbours => _neighbours;

        // Lista de pontos dentro do prefab
        [SerializeField]  private List<IKPositions> _iks = new List<IKPositions>();
        public List<IKPositions> Iks => _iks;

        public IKPositions ReturnIK(AvatarIKGoal goal){
            IKPositions returnIkVal = null;
            foreach (IKPositions ik in _iks)
            {
                if (ik.Ik == goal)
                {
                    returnIkVal = ik;
                    break;
                }
            }

            return returnIkVal;

        }

        public Neighbour ReturnNeighbour(Point target){
            // da lista de vizinhos retorna o alvo da função
            Neighbour returnNeighbourVal = null;
            foreach (Neighbour neighbour in _neighbours)
            {
                if (neighbour.Target == target)
                {
                    returnNeighbourVal = neighbour;
                    break;
                }
            }

            return returnNeighbourVal;

        }
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }
    }

    [System.Serializable]
    public class IKPositions {
        [SerializeField] private AvatarIKGoal _ik;
        public AvatarIKGoal Ik => _ik;

        [SerializeField] private Transform _target;
        public Transform Target => _target;

        [SerializeField] private Transform _hint;
        public Transform Hint => _hint;
        
    }

    [System.Serializable]
    public class Neighbour {
        [SerializeField] private Vector3 _direction;
        public Vector3 Direction {
            get => _direction;
            set => _direction = value;
        }

        [SerializeField] private Point _target;
        public Point Target {
            get => _target;
            set => _target = value;
        }

        [SerializeField] private EnumConnectionType _connectionType;
        public EnumConnectionType ConnectionType {
            get => _connectionType;
            set => _connectionType = value;
        }

        [SerializeField] private bool _customConnection;
        public bool CustomConnection {
            get => _customConnection;
            set => _customConnection = value;
        }
    }
    public enum EnumConnectionType
    {
        INBETWEEN, // transições vizinhas
        DIRECT, // necessidade de pulos
        DISMOUNT, // sobe ou sai para uma plataforma
        FALL
    }

    //public enum AvatarIKGoal
    //{
        //LeftFoot,
        //RightFoot,
        //LeftHand,
        //RightHand
    //}
}

