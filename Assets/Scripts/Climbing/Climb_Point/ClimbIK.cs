using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;

namespace Climbing
{
    public class ClimbIK : MonoBehaviour
    {
        
        private Animator _animator;

        [SerializeField] private bool _forceFeetHeight;

        private Transform _playerHips;

        #region Pontos IK
        private Point _LHandPoint;
        private Point _LFootPoint;
        private Point _RHandPoint;
        private Point _RFootPoint;
        #endregion

        #region Peso Iks
        [SerializeField] private float _LHandWeight = 1.0f;
        public float LHandWeight
        {
            get { return _LHandWeight; }
            set { _LHandWeight = value; }
        }

        [SerializeField] private float _LFootWeight = 1.0f;
        public float LFootWeight
        {
            get { return _LFootWeight; } 
            set { _LFootWeight = value; }
        }
        [SerializeField] private float _RFootWeight = 1.0f;
        public float RFootWeight
        {
            get { return _RFootWeight; }
            set { _RFootWeight= value; }
        }
        [SerializeField] private float _RHandWeight = 1.0f;
        public float RHandWeight
        {
            get { return _RHandWeight; }
            set { _RHandWeight = value; }
        }
        #endregion

        #region Helpers IKs

        private Transform _LHandHelper;
        private Transform _LFootHelper;
        private Transform _RHandHelper;
        private Transform _RFootHelper;

        [SerializeField] private float _helperSpeed;
        #endregion

        #region Iks Posi��o Alvo

        private Vector3 _LHandTargetPosition;
        private Vector3 _LFootTargetPosition;
        private Vector3 _RHandTargetPosition;
        private Vector3 _RFootTargetPosition;

        #endregion

        // Start is called before the first frame update
        void Start()
        {
            _animator = GetComponent<Animator>();
            _playerHips = _animator.GetBoneTransform(HumanBodyBones.Hips);

            _LHandHelper = new GameObject().transform;
            _LHandHelper.name = "lh helper ik";
            _LFootHelper = new GameObject().transform;
            _LFootHelper.name = "lf helper ik";
            _RFootHelper = new GameObject().transform;
            _RFootHelper.name = "rf helper ik";
            _RHandHelper = new GameObject().transform;
            _RHandHelper.name = "rh helper ik";
            
        }

        public void UpdateAllPointsOnOne(Point targetPoint) 
        {
            // atualiza os iks de acordo com o ponto alvo
            _LHandPoint = targetPoint;
            _LFootPoint = targetPoint;
            _RFootPoint = targetPoint;
            _RHandPoint = targetPoint;
        }

        public void UpdatePoint(AvatarIKGoal ik, Point targetPoint)
        {
            switch(ik)
            {
                case AvatarIKGoal.RightHand: 
                    _RHandPoint = targetPoint; 
                    break;
                case AvatarIKGoal.RightFoot: 
                    _RFootPoint = targetPoint; 
                    break;
                case AvatarIKGoal.LeftFoot: 
                    _LFootPoint = targetPoint;
                    break;
                case AvatarIKGoal.LeftHand:
                    _LHandPoint = targetPoint;
                    break;
                default:
                    break;
            }
        }

        public void UpdateAllTargetPositions(Point point)
        {
            // atualiza as posi��es alvo de acordo com o alvo
            IKPositions LHandHolder = point.ReturnIK(AvatarIKGoal.LeftHand);
            if (LHandHolder.Target)
                _LHandTargetPosition = LHandHolder.Target.position;

            IKPositions LFootHolder = point.ReturnIK(AvatarIKGoal.LeftFoot);
            if (LFootHolder.Target)
                _LFootTargetPosition = LFootHolder.Target.position;

            IKPositions RHandHolder = point.ReturnIK(AvatarIKGoal.RightHand);
            if(RHandHolder.Target)
                _RHandTargetPosition = RHandHolder.Target.position;

            IKPositions RFootHolder = point.ReturnIK(AvatarIKGoal.RightFoot);
            if(RFootHolder.Target)
                _RFootTargetPosition = RFootHolder.Target.position;
        }

        public void UpdateTargetPosition(AvatarIKGoal ik, Vector3 targetPosition )
        {
            switch (ik)
            {
                case AvatarIKGoal.RightHand:
                    _RHandTargetPosition = targetPosition;
                    break;
                case AvatarIKGoal.RightFoot:
                    _RFootTargetPosition = targetPosition;
                    break;
                case AvatarIKGoal.LeftFoot:
                    _LFootTargetPosition = targetPosition;
                    break;
                case AvatarIKGoal.LeftHand:
                    _LHandTargetPosition = targetPosition;
                    break;
                default:
                    break;
            }
        }

        public Vector3 ReturnCurrentPointPosition(AvatarIKGoal ik)
        {
            Vector3 currentPointPosition = default;

            switch (ik)
            {
                case AvatarIKGoal.RightHand:
                    IKPositions RHandHolder = _RHandPoint.ReturnIK(AvatarIKGoal.RightHand);
                    currentPointPosition = RHandHolder.Target.transform.position;
                    break;
                case AvatarIKGoal.RightFoot:
                    IKPositions RFootHolder = _RFootPoint.ReturnIK(AvatarIKGoal.RightFoot);
                    currentPointPosition = RFootHolder.Target.transform.position;
                    break;
                case AvatarIKGoal.LeftFoot:
                    IKPositions LFootHolder = _LFootPoint.ReturnIK(AvatarIKGoal.LeftFoot);
                    currentPointPosition = LFootHolder.Target.transform.position;
                    break;
                case AvatarIKGoal.LeftHand:
                    IKPositions LHandHolder = _LHandPoint.ReturnIK(AvatarIKGoal.LeftHand);
                    currentPointPosition = LHandHolder.Target.transform.position;
                    break;
                default:
                    break;
            }

            return currentPointPosition;
        }

        public Point ReturnPointForIK(AvatarIKGoal ik)
        {
            Point IKPoint = null;

            switch (ik)
            {
                case AvatarIKGoal.RightHand:
                    IKPoint = _RHandPoint;
                    break;
                case AvatarIKGoal.RightFoot:
                    IKPoint= _RFootPoint;
                    break;
                case AvatarIKGoal.LeftFoot:
                    IKPoint = _LFootPoint;
                    break;
                case AvatarIKGoal.LeftHand:
                    IKPoint=_LHandPoint;
                    break;
                default:
                    break;
            }

            return IKPoint;
        }

        public AvatarIKGoal ReturnOppositeIK(AvatarIKGoal ik)
        {
            AvatarIKGoal oppositeIK = default;

            switch (ik)
            {
                case AvatarIKGoal.RightHand:
                    oppositeIK = AvatarIKGoal.LeftHand;
                    break;
                case AvatarIKGoal.RightFoot:
                    oppositeIK = AvatarIKGoal.LeftFoot;
                    break;
                case AvatarIKGoal.LeftFoot:
                    oppositeIK = AvatarIKGoal.RightFoot;
                    break;
                case AvatarIKGoal.LeftHand:
                    oppositeIK = AvatarIKGoal.RightHand;
                    break;
                default:
                    break;
            }

            return oppositeIK;
        }

        public AvatarIKGoal ReturnOppositeLimb(AvatarIKGoal ik)
        {
            AvatarIKGoal oppositeLimb = default;

            switch (ik)
            {
                case AvatarIKGoal.RightHand:
                    oppositeLimb = AvatarIKGoal.RightFoot;
                    break;
                case AvatarIKGoal.RightFoot:
                    oppositeLimb = AvatarIKGoal.RightHand;
                    break;
                case AvatarIKGoal.LeftFoot:
                    oppositeLimb = AvatarIKGoal.LeftHand;
                    break;
                case AvatarIKGoal.LeftHand:
                    oppositeLimb = AvatarIKGoal.LeftFoot;
                    break;
                default:
                    break;
            }

            return oppositeLimb;
        }

        public void AddWeightInfluenceAll(float w)
        {
            LHandWeight = w;
            LFootWeight = w;
            RHandWeight = w;
            RFootWeight = w;
        }

        public void ImmediatePlaceHelpers()
        {
            // 
            if(_LHandPoint != null)
            {
                _LHandHelper.position = _LHandTargetPosition;
            }

            if (_LFootPoint != null)
            {
                _LFootHelper.position = _LFootTargetPosition;
            }

            if (_RHandPoint != null)
            {
                _RHandHelper.position = _RHandTargetPosition;
            }

            if (_RFootPoint != null)
            {
                _RFootHelper.position = _RFootTargetPosition;
            }
 
        }

        public void OnAnimatorIK()
        {
            /*
            // m�o esquerda
            if (_LHandPoint)
            {
                
                IKPositions LHandHolder = _LHandPoint.ReturnIK(AvatarIKGoal.LeftHand);

                if (LHandHolder.Target) 
                {
                    _LHandHelper.transform.position = Vector3.Lerp(_LHandHelper.transform.position, _LHandTargetPosition, Time.deltaTime * _helperSpeed);
                }

                UpdateIK(AvatarIKGoal.LeftHand, LHandHolder,_LHandHelper, _LHandWeight, AvatarIKHint.LeftElbow);
            }

            // m�o direita
            if (_RHandPoint)
            {

                IKPositions RHandHolder = _RHandPoint.ReturnIK(AvatarIKGoal.RightHand);

                if (RHandHolder.Target)
                {
                    _RHandHelper.transform.position = Vector3.Lerp(_RHandHelper.transform.position, _RHandTargetPosition, Time.deltaTime * _helperSpeed);
                }

                UpdateIK(AvatarIKGoal.RightHand, RHandHolder, _RHandHelper, _RHandWeight, AvatarIKHint.RightElbow);
            }

            // ajeita a vari�vel do quadril se ainda n�o colocada
            if(_playerHips ==  null)
                _playerHips = _animator.GetBoneTransform(HumanBodyBones.Hips); 
            
            // p� esquerdo
            if(_LFootPoint)
            {
                IKPositions LFootHolder = _LFootPoint.ReturnIK(AvatarIKGoal.LeftFoot);

                if (LFootHolder.Target)
                {
                    Vector3 targetPosition = _LFootTargetPosition;

                    // coloca os p�s abaixo do quadril, n�o funciona para rota��o TODO: ajeitar depois

                    if (_forceFeetHeight)
                    {
                        if (targetPosition.y > _playerHips.transform.position.y)
                        {
                            targetPosition.y = targetPosition.y - 0.2f;
                        }
                    }

                    _LFootHelper.transform.position = Vector3.Lerp(_LFootHelper.transform.position, targetPosition, Time.deltaTime * _helperSpeed);
                }

                UpdateIK(AvatarIKGoal.LeftFoot, LFootHolder, _LFootHelper, _LFootWeight, AvatarIKHint.LeftKnee);
            }

            // p� direito
            if (_RFootPoint)
            {
                IKPositions RFootHolder = _RFootPoint.ReturnIK(AvatarIKGoal.RightFoot);

                if (RFootHolder.Target)
                {
                    Vector3 targetPosition = _RFootTargetPosition;

                    // coloca os p�s abaixo do quadril, n�o funciona para rota��o TODO: ajeitar depois

                    if (_forceFeetHeight)
                    {
                        if (targetPosition.y > _playerHips.transform.position.y)
                        {
                            targetPosition.y = targetPosition.y - 0.2f;
                        }
                    }

                    _RFootHelper.transform.position = Vector3.Lerp(_RFootHelper.transform.position, targetPosition, Time.deltaTime * _helperSpeed);
                }

                UpdateIK(AvatarIKGoal.RightFoot, RFootHolder, _RFootHelper, _RFootWeight, AvatarIKHint.RightKnee);
            }
            */
        }

        private void UpdateIK(AvatarIKGoal ik, IKPositions holder,Transform helper, float weight, AvatarIKHint ikHint)
        {
            if(holder != null)
            {
                _animator.SetIKPositionWeight(ik, weight);
                _animator.SetIKRotationWeight(ik, weight);
                _animator.SetIKPosition(ik, helper.position);
                _animator.SetIKRotation(ik, helper.rotation);

                if (ik == AvatarIKGoal.LeftHand || ik == AvatarIKGoal.RightHand)
                {
                    Transform shoulder = (ik == AvatarIKGoal.LeftHand) ? _animator.GetBoneTransform(HumanBodyBones.LeftShoulder) : _animator.GetBoneTransform(HumanBodyBones.RightShoulder);

                    Vector3 targetRotationDirection = shoulder.transform.position - helper.transform.position;
                    Quaternion targetRotation = Quaternion.LookRotation(-targetRotationDirection);
                    helper.rotation = targetRotation;
                }
                else
                {
                    helper.rotation = holder.Target.transform.rotation;
                }

                if(holder.Hint != null)
                {
                    _animator.SetIKHintPositionWeight(ikHint, weight);
                    _animator.SetIKHintPosition(ikHint, helper.position);
                }
            }
        }

        public void InfluenceWeight(AvatarIKGoal ik, float weight)
        {
            switch (ik)
            {
                case AvatarIKGoal.LeftHand:
                    _RHandWeight = weight;
                    break;
                case AvatarIKGoal.RightHand: 
                    _RHandWeight = weight; 
                    break;
                case AvatarIKGoal.RightFoot: 
                    _RFootWeight = weight;
                    break;
                case AvatarIKGoal.LeftFoot:
                    _LFootWeight = weight;
                    break;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
