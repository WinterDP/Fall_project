#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Climbing
{
    [ExecuteInEditMode]
    public class DrawWireCube : MonoBehaviour
    {
        [SerializeField] public List<IKPositions> _ikPos = new List<IKPositions>();
        public List<IKPositions> IkPos  {
            get => _ikPos;
            set => _ikPos = value;
        }

        public bool refresh = false;

        // Update is called once per frame
        void Update()
        {
            if(refresh){
                _ikPos.Clear();
                refresh = false;
            }
        }
    }
} 
#endif


