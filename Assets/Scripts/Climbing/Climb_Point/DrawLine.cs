#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Climbing
{
    [ExecuteInEditMode]
    public class DrawLine : MonoBehaviour
    {
            [SerializeField] private List<Connection> _connectedPoints = new List<Connection>();
            public List<Connection> ConnectedPoints => _connectedPoints;

            [SerializeField] private bool _refresh = false;
            public bool Refresh  {
                get => _refresh;
                set => _refresh = value;
            } 

            // Update is called once per frame
            void Update()
            {
                if(_refresh){
                    _connectedPoints.Clear();
                    _refresh = false;
                }
            }
    }
  
}
#endif  