using UnityEngine;

public class ButtonInteraction : MonoBehaviour
{
    public float interactionDistance = 2f;
    public LayerMask leverLayer;
    public string leverTag = "Button";
    public GameObject door;
    public GameObject Button;
    public Animator ButtonAnimator;

    private bool isInteracting = false;
    private bool leverPulled = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!isInteracting)
            {
                // Verifica se há uma alavanca próxima ao jogador com a tag correta
                Collider[] colliders = Physics.OverlapSphere(transform.position, interactionDistance, leverLayer);
                if (colliders.Length > 0)
                {
                    foreach (Collider collider in colliders)
                    {
                        if (collider.CompareTag(leverTag))
                        {
                            // Seleciona a alavanca para interação
                            isInteracting = true;
                            //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                            // Realiza ação da alavanca (exemplo: mudar a cor do objeto)
                            ButtonAnimator.Play("DoorButton");
                            //collider.GetComponent<Renderer>().material.color = Color.red;

                            if(!AudioManager.instance.IsPlaying("Door_Opening")){
                                 AudioManager.instance.PlaySound("button");
                                 AudioManager.instance.PlaySound("Door_Opening");
                            }
                            // Executa evento da alavanca (exemplo: abrir uma porta)
                            OpenDoor();

                            break;
                        }
                    }
                }
            }
            else
            {
                // Interrompe a interação
                isInteracting = false;
            }
        }
    }

    /*void Start()
    {
        // Registra o método "OnAnimationComplete" como um evento de animação
        ButtonAnimator.GetBehaviour<AnimationEventBehaviour>().onAnimationComplete += OnAnimationComplete;
    }*/

    /* OnAnimationComplete()
    {
        // Destrói o objeto da porta após a animação de abertura ser concluída
        Destroy(door);
    }*/

    private void OpenDoor()
    {
        // Implemente a ação que deseja realizar quando a alavanca for puxada
        
        if(!leverPulled){
            
            //Destroy(door);
            Debug.Log("Porta aberta");
            leverPulled = true;
        }
        
        
    }
    

}
