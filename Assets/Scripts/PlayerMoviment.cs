using UnityEngine;

public class PlayerMoviment : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float jumpForce = 10f;
    public Rigidbody rigidbody;

    private bool isGrounded = true;

    public float checkDistance;
    private float bufferCheckDistance =0.1f;


    void Update()
    {

        checkDistance = (GetComponent<BoxCollider>().size.y / 2) + bufferCheckDistance;

        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // Movimentação do personagem
        Vector3 movement = new Vector3(horizontalInput, 0f, verticalInput) * moveSpeed * Time.deltaTime;
        transform.Translate(movement, Space.Self);

        // Verifica se o personagem está no chão
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.up, out hit, checkDistance))
        {
            isGrounded = true;
        }
        else
        {
           isGrounded = false;
        }

        // Verifica se a tecla espaço foi pressionada para pular
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }
    }
}
