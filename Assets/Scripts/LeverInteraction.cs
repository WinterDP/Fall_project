using UnityEngine;

public class LeverInteraction : MonoBehaviour
{
    public float interactionDistance = 2f;
    public LayerMask leverLayer;
    public string leverTag = "Lever";
    public GameObject door;
    public GameObject lever;
    public Animator leverAnimator;

    private bool isInteracting = false;
    private bool leverPulled = false;

    void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            //Debug.Log("F");
            if (!isInteracting)
            {
            //Debug.Log("G");

                // Verifica se há uma alavanca próxima ao jogador com a tag correta
                Collider[] colliders = Physics.OverlapSphere(transform.position, interactionDistance, leverLayer);
                if (colliders.Length > 0)
                {
            //Debug.Log("H");

                    foreach (Collider collider in colliders)
                    {
                        if (collider.CompareTag(leverTag))
                        {
            //Debug.Log("I");

                            // Seleciona a alavanca para interação
                            isInteracting = true;
                            //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                            // Realiza ação da alavanca (exemplo: mudar a cor do objeto)
                            leverAnimator.Play("Ativado");
                            //collider.GetComponent<Renderer>().material.color = Color.red;
                            Destroy(door);
                            Debug.Log("Alavanca ativada");
                            if(!AudioManager.instance.IsPlaying("Door_Opening")){

                                AudioManager.instance.PlaySound("lever");
                                 AudioManager.instance.PlaySound("Door_Opening");
                            }
                            // Executa evento da alavanca (exemplo: abrir uma porta)
                            OpenDoor();

                            break;
                        }
                    }
                }
            }
            else
            {
                // Interrompe a interação
                isInteracting = false;
            }
        }
    }

    private void OpenDoor()
    {
        // Implemente a ação que deseja realizar quando a alavanca for puxada
        
        if(!leverPulled){
            
            
            Debug.Log("Porta aberta");
            leverPulled = true;
        }
        
        
    }
    

}
