using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ThirdPersonCameraController : MonoBehaviour
{
    public float RotationSpeed = 1;

    public Transform Target, Player;

    private float mouseX, mouseY;

    public List<Transform> Obstructions = new List<Transform>();
    public Transform Obstruction;
    public Transform Previous;
    private float zoomSpeed = 2f;
    
    // Start is called before the first frame update
    void Start()
    {
        Obstruction = Target;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LateUpdate()
    {
       CamControl();
       ViewObstruction();
    }

    void CamControl()
    //Aqui é foda-se, o que fizer melhor ta bala
    {
        mouseX += Input.GetAxis("Mouse X") * RotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * RotationSpeed;
        mouseY = Mathf.Clamp(mouseY, -35, 60);
        
        transform.LookAt(Target);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
        }
        else
        {
            Target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
            Player.rotation = Quaternion.Euler(0, mouseX, 0);
        }
        
    }

    void ViewObstruction()
    //Aqui serve pra apagar qualquer obstrução que apareca entre a camera e o target(player)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Target.position - transform.position, out hit,
                12f))//Envia um raycast da camera até o Target, que é o player
        {
            if (hit.collider.gameObject.tag != "Player")//Do jeito que tá agora, ele analisa a obstrução pela Tag
            {
                Previous = Obstruction;
                Obstruction = hit.transform;
                 if (Previous != Obstruction) //Se a obstrução mudou, salva a anterior em uma lista
                 {
                     Obstructions.Add(Previous);
                     // Previous.gameObject.GetComponent<MeshRenderer>().shadowCastingMode =
                     //     UnityEngine.Rendering.ShadowCastingMode.On;  
                 }
                Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode =
                    UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;//Apaga o renderer da obstrução
                
                //Aumentar o zoom da camera
                // if (Vector3.Distance(Obstruction.position, transform.position) >= 3f &&
                //     Vector3.Distance(transform.position, Target.position) > 1.5)
                // {
                //     transform.Translate(Vector3.forward * zoomSpeed * Time.deltaTime);
                // }
            }
            else
            {
                Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode =
                    UnityEngine.Rendering.ShadowCastingMode.On; //Liga o renderer da obstrução
                foreach (Transform obs in Obstructions) //Liga o renderer das outras obstruções na lista
                {
                    obs.gameObject.GetComponent<MeshRenderer>().shadowCastingMode =
                        UnityEngine.Rendering.ShadowCastingMode.On;
                }
                //Redefinir o zoom da camera
                // if(Vector3.Distance(transform.position, Target.position) < 12f)
                // {
                //     transform.Translate(Vector3.back * zoomSpeed * Time.deltaTime);
                // }
            }
        }
    }
}
