using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Creditos : MonoBehaviour
{   
    [SerializeField] private string nome_cena;
    // Start is called before the first frame update
    private void Start()
    {
        if(!AudioManager.instance.IsPlaying("MainTheme"))
            AudioManager.instance.PlaySound("MainTheme");

        Cursor.visible = true;

    }
    public void Back()
    {
        SceneManager.LoadScene(nome_cena);
        if (nome_cena == "CocoVilela")
        {
            AudioManager.instance.StopSound("MainTheme");
            AudioManager.instance.PlaySound("Ambiente");
        }
        //AudioManager.instance.StopSound("MainTheme");
        Debug.Log(nome_cena + " foi requisitada");
    }
}
