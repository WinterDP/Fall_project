using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public float interactionDistance = 2f;
    public LayerMask interactableLayer;
    public string interactableTag = "Interactable";
    public Transform interactionPoint;
    public float pullForce = 10f;

    private bool isInteracting = false;
    private Rigidbody interactableObject;
    private Vector3 initialOffset;
    private bool originalKinematicState;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!isInteracting)
            {
                // Verifica se há um objeto interativo próximo ao jogador com a tag correta
                Collider[] colliders = Physics.OverlapSphere(interactionPoint.position, interactionDistance, interactableLayer);
                if (colliders.Length > 0)
                {
                    foreach (Collider collider in colliders)
                    {
                        if (collider.CompareTag(interactableTag))
                        {
                            // Seleciona o objeto para puxar
                            interactableObject = collider.GetComponent<Rigidbody>();
                            isInteracting = true;

                            // Calcula o deslocamento inicial entre o jogador e o objeto
                            initialOffset = transform.position - interactableObject.transform.position;

                            // Salva o estado original de isKinematic do objeto
                            //originalKinematicState = interactableObject.isKinematic;

                            // Desativa temporariamente a física do objeto
                            //interactableObject.isKinematic = false;
                            
                            break;
                        }
                    }
                }
            }
            else
            {
                // Interrompe a interação
                //interactableObject.isKinematic = false;
                interactableObject = null;
                isInteracting = false;
            }
        }

        
    }

    private void FixedUpdate()
    {
        if (isInteracting && interactableObject != null)
        {
            // Calcula a direção entre o jogador e o objeto
            Vector3 pullDirection = (transform.position - interactableObject.transform.position).normalized;

            // Calcula a posição atual do jogador em relação ao objeto
            Vector3 targetPosition = interactableObject.transform.position + initialOffset;

            // Move o jogador para a posição alvo
            transform.position = targetPosition;

            // Rotaciona o jogador para olhar na direção do objeto
            transform.LookAt(interactableObject.transform);

            // Aplica a força de puxar no objeto
            interactableObject.AddForce(pullDirection * pullForce, ForceMode.Force);
        }
    }

    private void OnDrawGizmosSelected()
    {
        // Desenha uma esfera gizmo para representar a área de interação
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionPoint.position, interactionDistance);
    }
}
