﻿using UnityEngine;
using System.Collections;

namespace Controller
{
    public class HandleMovement : MonoBehaviour
    {

        public Rigidbody rb;
        StateManager states;


        public float interactionDistance = 20f;
        public LayerMask interactableLayer;
        public string interactableTag = "Interactable";
        public Transform interactionPoint;
        float pullForce = 10f;

        private bool isInteracting = false;
        private Rigidbody interactableObject;
        private Vector3 initialOffset;
        private bool originalKinematicState;




        InputHandler ih;
        private float moveSpeed;
        [SerializeField] private float moveSpeedNormal;
        [SerializeField] private float moveSpeedSprint;
        [SerializeField] private float rotateSpeed;
        [SerializeField] private float jumpForce;

        Vector3 storeDirection;

        public void Init()
        {
            states = GetComponent<StateManager>();
            rb = GetComponent<Rigidbody>();
            ih = GetComponent<InputHandler>();

            //rb.angularDrag = 999;
            //rb.drag = 4;
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }

        public void Tick()
        {
            Vector3 v = ih.camHolder.forward.normalized * states.vertical;
            Vector3 h = ih.camHolder.right.normalized * states.horizontal;

            v.y = 0;
            h.y = 0;

            if (states.onGround)
            {
                rb.AddForce((v + h).normalized * moveSpeed);
            }
            else
            {
                
                rb.AddForce(0.3f * moveSpeed * (v + h).normalized);
            }


            if (states.jump && !states.stopJump){
                //rb.velocity = new Vector3(rb.velocity.x, Vector3.up.y * jumpForce, rb.velocity.z);
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                //if (!states.stopJump)
                //{
                    //rb.velocity = new Vector3(rb.velocity.x, 2 * Vector3.up.y * jumpForce, rb.velocity.z);
                //}
            }

                
       


            if (states.sprint && states.onGround)
            {
                
                moveSpeed = moveSpeedSprint;
            }

            if (!states.sprint && states.onGround) 
            {
                moveSpeed = moveSpeedNormal;
            }

            if(Mathf.Abs(states.vertical) > 0 || Mathf.Abs(states.horizontal) > 0)
            {
                storeDirection = (v + h).normalized;

                storeDirection += transform.position;

                Vector3 targetDir = (storeDirection - transform.position).normalized;
                targetDir.y = 0;

                if (targetDir == Vector3.zero)
                    targetDir = transform.forward;

                Quaternion targetRot = Quaternion.LookRotation(targetDir);
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, rotateSpeed * Time.deltaTime);
            }

            /*if(states.interaction){
                interaction();
            }*/
            if (states.onGround && (v.x != 0 || h.x != 0))
            {
                if (states.sprint)
                {
                    if (!AudioManager.instance.IsPlaying("FootsSteps_running"))
                    {
                        AudioManager.instance.StopSound("FootsSteps");
                        AudioManager.instance.PlaySound("FootsSteps_running");
                    }
                }
                else
                {
                    if (!AudioManager.instance.IsPlaying("FootsSteps"))
                    {
                        AudioManager.instance.StopSound("FootsSteps_running");
                        AudioManager.instance.PlaySound("FootsSteps");
                    }
                }
            }
            else
            {
                AudioManager.instance.StopSound("FootsSteps_running");
                AudioManager.instance.StopSound("FootsSteps");
            }

        }

        void interaction(){
            if (!isInteracting)
            {
                // Verifica se há um objeto interativo próximo ao jogador com a tag correta
                Collider[] colliders = Physics.OverlapSphere(interactionPoint.position, interactionDistance, interactableLayer);
                if (colliders.Length > 0)
                {
                    foreach (Collider collider in colliders)
                    {
                        if (collider.CompareTag(interactableTag))
                        {
                            // Seleciona o objeto para puxar
                            interactableObject = collider.GetComponent<Rigidbody>();
                            isInteracting = true;

                            // Calcula o deslocamento inicial entre o jogador e o objeto
                            initialOffset = transform.position - interactableObject.transform.position;

                             // Salva o estado original de isKinematic do objeto
                            originalKinematicState = interactableObject.isKinematic;

                            // Desativa temporariamente a física do objeto
                            interactableObject.isKinematic = false;

                            break;
                        }
                    }
                }
            }
            else
            {
                // Interrompe a interação
                interactableObject.isKinematic = originalKinematicState;
                interactableObject = null;
                isInteracting = false;
            }
        

            if (isInteracting && interactableObject != null)
            {
                // Calcula a direção entre o jogador e o objeto
                Vector3 pullDirection = (transform.position - interactableObject.transform.position).normalized;

                // Calcula a posição atual do jogador em relação ao objeto
                Vector3 targetPosition = interactableObject.transform.position + initialOffset;

                // Move o jogador para a posição alvo
                transform.position = targetPosition;

                // Rotaciona o jogador para olhar na direção do objeto
                transform.LookAt(interactableObject.transform);

                // Aplica a força de puxar no objeto
                interactableObject.AddForce(pullDirection * pullForce, ForceMode.Force);

                //moveSpeed = 5f;

            }
            
        }
        

        float Speed()
        {
            return moveSpeed;
        }

    }
}
