﻿using UnityEngine;
using System.Collections;
//using static UnityEditor.VersionControl.Asset;

namespace Controller
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(HandleAnim))]
    public class StateManager : MonoBehaviour
    {
        public float horizontal;
        public float vertical;
        public bool dummy;
        public bool onGround = true;
        public bool jump = false;
        public bool stopJump;
        public bool sprint;
        public bool interaction;
        public bool isTired = false;
        public bool CanJump;
        
        [SerializeField] private float _coyoteTime;
        private float _coyoteTimeCounter;

        [SerializeField] public float _jumpBufferTime;
        public float _jumpBufferTimeCounter;

        [HideInInspector]
        public HandleAnim hAnim;
        [HideInInspector]
        public HandleMovement hMovement;

        void Start()
        {
            hAnim = GetComponent<HandleAnim>();
            hMovement = GetComponent<HandleMovement>();

            hAnim.Init(this);
            hMovement.Init();
        }
        private void Update()
        {
            if (!dummy)
            {
                IsOnGround();
                PlayerCanJump();
                if (stopJump)
                {
                    _coyoteTimeCounter = 0;
                }
            }   
        }

        void FixedUpdate()
        {
            if (!dummy)
            {
                hAnim.Tick();
                hMovement.Tick();

            }
            else
            {
                AudioManager.instance.StopSound("FootsSteps");
                AudioManager.instance.StopSound("FootsSteps_running");

            }
        }

        public void EnableController()
        {
            dummy = false;
            hMovement.rb.isKinematic = false;
            GetComponent<Collider>().isTrigger = false;
        }

        public void DisableController()
        {
            // usado para travar o controle do player enquanto ele está na parede
            dummy = true;
            hMovement.rb.isKinematic = true;
            GetComponent<Collider>().isTrigger = true;
        }

        void IsOnGround()
        {
            onGround = OnGround();

            if(onGround)
            {
                hAnim.anim.SetBool("onAir", false);
                hMovement.rb.drag = 10;
                stopJump = false;
            }

        }

        public void PlayerCanJump()
        {
            if (onGround)
            {
                CanJump = true;
                _coyoteTimeCounter = _coyoteTime;
                return;
            }
            else
            {
                _coyoteTimeCounter -= Time.deltaTime;
                if (_coyoteTimeCounter > 0f && _jumpBufferTimeCounter > 0f)
                {
                    CanJump = true;
                    return;
                }
                else
                {
                    hAnim.anim.SetBool("onAir", true);
                    hMovement.rb.drag = 0;
                    CanJump = false;
                    return;
                }
                
            }
        }

        bool OnGround()
        {
            bool retVal = false;
            
            Vector3 origin = transform.position + Vector3.up / 18;
            Vector3 direction = -Vector3.up;
            float distance =  0.2f;
            LayerMask lm = ~(1 << gameObject.layer);
            RaycastHit hit;

            if(Physics.Raycast(origin, direction, out hit, distance, lm))
            {
                if (hit.transform.gameObject.layer == gameObject.layer)
                    Debug.Log("OnGround hit an object with the same layer as the controller!!");

                retVal = true;
            }

            if (!CanJump && retVal)
            {
                if (!AudioManager.instance.IsPlaying("JumpFall"))
                {
                    AudioManager.instance.PlaySound("JumpFall");
                }
            }

            return retVal;
        }
    }
}
