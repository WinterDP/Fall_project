﻿using UnityEngine;
using System.Collections;
using Controller;
using UnityEngine.SceneManagement;
using UnityEngine.PlayerLoop;

[RequireComponent(typeof(StateManager))]
[RequireComponent(typeof(HandleMovement))]
public class InputHandler : MonoBehaviour {

    StateManager states;
    //public bool jump;

    [HideInInspector]
    public Transform camHolder;

	void Start () {
        
        states = GetComponent<StateManager>();
        camHolder = Camera.main.transform;
    }
	void Update () {
        HandleAxis();
        isJumping();
        isSprinting();
        isInteracting();
        Cursor.visible = false;
    }

    void isSprinting(){
        if (states.onGround && !states.isTired && (states.vertical != 0 || states.horizontal != 0))
        {
            states.sprint = Input.GetKey(KeyCode.LeftShift);
        }
        else
        {
            states.sprint = false;
        }
            
    }

    void isInteracting(){
        states.interaction = Input.GetKey(KeyCode.E);
    }

    void HandleAxis()
    {
        states.horizontal = Input.GetAxis("Horizontal");
        states.vertical = Input.GetAxis("Vertical");
    }

    void isJumping(){

        if (states.CanJump && !states.isTired)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (!AudioManager.instance.IsPlaying("Jump"))
                    AudioManager.instance.PlaySound("Jump");
                gameObject.GetComponent<PlayerStamina>().SpendStamina(10f);
                states._jumpBufferTimeCounter = states._jumpBufferTime;
                states.jump = true;
                states.stopJump = false;
            }
            else
            {
                states._jumpBufferTimeCounter -= Time.deltaTime;
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                states.stopJump = true;
                states.jump = false;
            }
        }
        else
        {
            states.jump = false;
        }
    }

    void ChangeSceneBeta()
    {
        if(Input.GetKey(KeyCode.C))
        {
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;
            string[] scenes = new string[sceneCount];
            for (int i = 0; i < sceneCount; i++)
            {
                scenes[i] = System.IO.Path.GetFileNameWithoutExtension(UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i));
            }

            foreach (var scene in scenes)
            {
                if (scene != SceneManager.GetActiveScene().name)
                {
                    SceneManager.LoadScene(scene);
                }
            }
        }


        
    }
}
