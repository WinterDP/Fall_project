#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Climbing
{
    [CustomEditor(typeof(DrawWireCube))]
    public class DrawWireCubeEditor : Editor{
        private void OnSceneGUI() {
            DrawWireCube t = target as DrawWireCube;
        

            if (t.IkPos.Count > 0)
            {
                t.IkPos = t.transform.GetComponent<Point>().Iks;
            }

            foreach (IKPositions ik in t.IkPos)
            {
                if (ik.Target != null)
                {
                    Color targetColor = Color.red;

                    switch (ik.Ik)
                    {
                        case AvatarIKGoal.LeftFoot:
                            targetColor = Color.magenta;
                            break;
                        case AvatarIKGoal.LeftHand:
                            targetColor = Color.cyan;
                            break;
                        case AvatarIKGoal.RightFoot:
                            targetColor = Color.green;
                            break;
                        case AvatarIKGoal.RightHand:
                            targetColor = Color.yellow;
                            break;
                    }

                    Handles.color = targetColor;
                    Handles.CubeHandleCap(0, ik.Target.position, ik.Target.rotation, 0.05f, EventType.Repaint );

                    if (ik.Hint != null)
                    {
                        Handles.CubeHandleCap(0, ik.Hint.position, ik.Hint.rotation, 0.05f, EventType.Repaint);
                    }
                }else{
                    t.IkPos = t.transform.GetComponent<Point>().Iks;
                }
            }
        }
    }
    
    [CustomEditor(typeof(DrawLine))]
    public class EditorVisEditor : Editor{
        private void OnSceneGUI() {
            DrawLine t = target as DrawLine;

            if (t == null)
            {
                return;
            }

            if(t.ConnectedPoints.Count == 0){
                t.ConnectedPoints.AddRange(t.transform.GetComponent<HandlePointConnections>().GetAllConnections());
            }

            foreach (Connection ConnectedPoint in t.ConnectedPoints)
            {
                if (ConnectedPoint == null)
                {
                    continue;
                }

                Vector3 pos1 = ConnectedPoint.Target1.transform.position;
                Vector3 pos2 = ConnectedPoint.Target2.transform.position;

                switch (ConnectedPoint.ConnectionType)
                {
                    case EnumConnectionType.DIRECT:
                        // Transições mais longe
                        Handles.color = Color.red;
                        break;
                    case EnumConnectionType.INBETWEEN:
                        // transições mais perto
                        Handles.color = Color.green;
                        break;
                }
                
                Handles.DrawLine(pos1,pos2);
                t.Refresh = false;
            }

        }
    }
    

    [CustomEditor(typeof(DrawLineIndividual))]
    public class DrawLineVisEditor : Editor{
        private void OnSceneGUI() {
            DrawLineIndividual t = target as DrawLineIndividual;

            if (t == null)
            {
                return;
            }

            if(t.ConnectedPoints.Count == 0){
                t.ConnectedPoints.AddRange(t.transform.GetComponent<Point>().Neighbours);
            }

            foreach (Neighbour ConnectedPoint in t.ConnectedPoints)
            {
                if (ConnectedPoint == null)
                {
                    continue;
                }

                Vector3 pos1 = t.transform.position;
                Vector3 pos2 = ConnectedPoint.Target.transform.position;

                switch (ConnectedPoint.ConnectionType)
                {
                    case EnumConnectionType.DIRECT:
                        // Transições mais longe
                        Handles.color = Color.red;
                        break;
                    case EnumConnectionType.INBETWEEN:
                        // transições mais perto
                        Handles.color = Color.green;
                        break;
                }
                
                Handles.DrawLine(pos1,pos2);
                t.Refresh = false;
            }

        }
    }
}
#endif
