using UnityEngine;
using UnityEngine.SceneManagement;

public class Final : MonoBehaviour
{
    public string sceneName; // Nome da cena que você deseja carregar

    private void OnTriggerEnter(Collider other)
    {
        AudioManager.instance.StopSound("FootsSteps");
        AudioManager.instance.StopSound("Ambiente");
        AudioManager.instance.StopSound("FootsSteps_running");
        SceneManager.LoadScene(sceneName); // Carrega a cena com o nome especificado
    }
}
